# Minimal Einstein Toolkit: An Einstein Toolkit library for Numerical Relativity

... because reinventing the wheel is so 2000s.

## Authors

Zach Etienne & Roland Haas

## Current features

Apparent horizon finding, via AHFinderDirect.

## Instructions:
 
Run compile_script to compile the library and link against NRPy+ code BSSN/UIUCBlackHole_Playground.c.

ETK_interface.c contains the code linking the Toolkit to other numerical relativity codes (currently NRPy+).

To interface with other numerical relativity codes, you'll need to

1. Interpolate ADM spacetime variables onto a uniform Cartesian grid (large grids are not necessary when finding apparent horizons, for example), and then
2. Within ETK_interface.c, copy the data to Einstein Toolkit gridfunctions.

ETK_interface.c calls the Einstein Toolkit.

## TODO:

* Enable pseudo-time-evolution, so that each time the ETK library is called, it increments the time appropriately and runs CCTK_ANALYSIS. This would enable us to use pretracking to speed up apparent horizon finds. Only when the finalizeCactus() routine is called will it free the allocated gridfunctions and end the pseudo-time-evolution.
* Add option to disable all the output to the screen.
* Remove unused ETK functions



