/*
 *  Original SymPy expressions:
 *  "[parity[0] = 1,
 *    parity[1] = 1,
 *    parity[2] = 1,
 *    parity[3] = 1,
 *    parity[4] = 1,
 *    parity[5] = 1,
 *    parity[6] = 1,
 *    parity[7] = 1,
 *    parity[8] = 1,
 *    parity[9] = 1]"
 */
{
   parity[0] = 1;
   parity[1] = 1;
   parity[2] = 1;
   parity[3] = 1;
   parity[4] = 1;
   parity[5] = 1;
   parity[6] = 1;
   parity[7] = 1;
   parity[8] = 1;
   parity[9] = 1;
}
