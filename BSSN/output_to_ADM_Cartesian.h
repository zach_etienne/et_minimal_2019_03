
void output_to_ADM_Cartesian(const int Nxx[3],const int Nxx_plus_2NGHOSTS[3], 
                             REAL *xx[3], REAL *in_gfs, REAL *aux_gfs) {
#pragma omp parallel for
for(int i2=NGHOSTS; i2<NGHOSTS+Nxx[2]; i2++) {
    const REAL xx2 = xx[2][i2];
    for(int i1=NGHOSTS; i1<NGHOSTS+Nxx[1]; i1++) {
        const REAL xx1 = xx[1][i1];
        for(int i0=NGHOSTS; i0<NGHOSTS+Nxx[0]; i0++) {
            const REAL xx0 = xx[0][i0];
               /* 
                * NRPy+ Finite Difference Code Generation, Step 1 of 1: Read from main memory and compute finite difference stencils:
                */
               const double hDD00 = in_gfs[IDX4(HDD00GF, i0,i1,i2)];
               const double hDD01 = in_gfs[IDX4(HDD01GF, i0,i1,i2)];
               const double hDD02 = in_gfs[IDX4(HDD02GF, i0,i1,i2)];
               const double hDD11 = in_gfs[IDX4(HDD11GF, i0,i1,i2)];
               const double hDD12 = in_gfs[IDX4(HDD12GF, i0,i1,i2)];
               const double hDD22 = in_gfs[IDX4(HDD22GF, i0,i1,i2)];
               const double aDD00 = in_gfs[IDX4(ADD00GF, i0,i1,i2)];
               const double aDD01 = in_gfs[IDX4(ADD01GF, i0,i1,i2)];
               const double aDD02 = in_gfs[IDX4(ADD02GF, i0,i1,i2)];
               const double aDD11 = in_gfs[IDX4(ADD11GF, i0,i1,i2)];
               const double aDD12 = in_gfs[IDX4(ADD12GF, i0,i1,i2)];
               const double aDD22 = in_gfs[IDX4(ADD22GF, i0,i1,i2)];
               const double trK = in_gfs[IDX4(TRKGF, i0,i1,i2)];
               const double cf = in_gfs[IDX4(CFGF, i0,i1,i2)];
               /* 
                * NRPy+ Finite Difference Code Generation, Step 2 of 1: Evaluate SymPy expressions and write to main memory:
                */
               const double tmp0 = pow(cf, -2);
               const double tmp1 = tmp0*(hDD00 + 1);
               const double tmp2 = hDD01*tmp0;
               const double tmp3 = hDD02*tmp0;
               const double tmp4 = tmp0*(hDD11 + 1);
               const double tmp5 = hDD12*tmp0;
               const double tmp6 = tmp0*(hDD22 + 1);
               const double tmp7 = (1.0/3.0)*trK;
               aux_gfs[IDX4(GXXGF, i0, i1, i2)] = tmp1;
               aux_gfs[IDX4(GXYGF, i0, i1, i2)] = tmp2;
               aux_gfs[IDX4(GXZGF, i0, i1, i2)] = tmp3;
               aux_gfs[IDX4(GYYGF, i0, i1, i2)] = tmp4;
               aux_gfs[IDX4(GYZGF, i0, i1, i2)] = tmp5;
               aux_gfs[IDX4(GZZGF, i0, i1, i2)] = tmp6;
               aux_gfs[IDX4(KXXGF, i0, i1, i2)] = aDD00*tmp0 + tmp1*tmp7;
               aux_gfs[IDX4(KXYGF, i0, i1, i2)] = aDD01*tmp0 + tmp2*tmp7;
               aux_gfs[IDX4(KXZGF, i0, i1, i2)] = aDD02*tmp0 + tmp3*tmp7;
               aux_gfs[IDX4(KYYGF, i0, i1, i2)] = aDD11*tmp0 + tmp4*tmp7;
               aux_gfs[IDX4(KYZGF, i0, i1, i2)] = aDD12*tmp0 + tmp5*tmp7;
               aux_gfs[IDX4(KZZGF, i0, i1, i2)] = aDD22*tmp0 + tmp6*tmp7;
            
            
        } // END LOOP: for(int i0=NGHOSTS; i0<NGHOSTS+Nxx[0]; i0++)
    } // END LOOP: for(int i1=NGHOSTS; i1<NGHOSTS+Nxx[1]; i1++)
} // END LOOP: for(int i2=NGHOSTS; i2<NGHOSTS+Nxx[2]; i2++)
}
