 /*@@
   @file      flesh.cc
   @date      Fri Sep 18 14:17:08 1998
   @author    Tom Goodale
   @desc
   Main program file for cactus.
   @enddesc
   @version $Header$
 @@*/
#include <stdio.h>
#include <string.h>

#include "cctk_Flesh.h"
#include "cctk_GroupsOnGH.h"
#include "CactusMainFunctions.h"

//#include "CurviBoundaryConditions/gridfunction_defines.h"

static const char *params = NULL;

static tFleshConfig ConfigData;

static cGH *initCactus(char *argv0,CCTK_REAL *ADM_gridfunctions)
{
  /* fake some command line arguments for Cactus */
  int argc = 2;
  char devnull[] = "/dev/null";
  char *argv_data[] = {argv0, devnull, NULL};
  char **argv = &argv_data[0];

  /* Initialise any cactus specific stuff.
   */
  CCTKi_InitialiseCactus(&argc, &argv, &ConfigData);
  /* This is a (c-linkage) routine which has been registered by a thorn.
   */
  CCTK_Initialise(&ConfigData);

  return ConfigData.GH[0]; /* technically GH on convergence level 0 */
}

static void finalizeCactus()
{
  /* This is a (c-linkage) routine which has been registered by a thorn.
   */
  CCTK_Shutdown(&ConfigData);

  /* Shut down any cactus specific stuff.
   */
  CCTKi_ShutdownCactus(&ConfigData);
}

/* take over parfile handling from Cactus */
extern "C"
int cctk_PirahaParser(const char *buffer,unsigned long buffersize,int (*set_function)(const char *, const char *, int));
extern "C"
int ParseFile(FILE *ifp,
              int (*set_function)(const char *, const char *, int),
              tFleshConfig *ConfigData)
{
  return cctk_PirahaParser(params, strlen(params), set_function);
}

/* take over main() from Cactus */
int ETK_interface(int *argc, char ***argv,CCTK_REAL *ADM_gridfunctions)
{
  /* set some parameters to show how it's done */
  params =
    "ActiveThorns = \"PUGH SymBase CartGrid3D\"\n"
    "cactus::cctk_itlast = 0\n"
    "Driver::global_nsize = 70\n"
    "Driver::ghost_size = 3\n"
    "Driver::info = load\n"
    "grid::type = byrange\n"
    "grid::xmin = -1.078125000000000e+00\n"
    "grid::xmax = +1.078125000000000e+00\n"
    "grid::ymin = -1.078125000000000e+00\n"
    "grid::ymax = +1.078125000000000e+00\n"
    "grid::zmin = -1.078125000000000e+00\n"
    "grid::zmax = +1.078125000000000e+00\n"
    "ActiveThorns = ADMBase\n"
    "ActiveThorns = \"AHFinderDirect SphericalSurface SpaceMask StaticConformal IOUtil AEILocalInterp PUGHInterp PUGHReduce\"\n"
    "ADMBase::metric_type = \"physical\"\n"
    "IOUtil::out_dir = \"NRPy-output\"\n"
    "AHFinderDirect::find_every                             = 1\n"
    "AHFinderDirect::geometry_interpolator_name             = \"Lagrange polynomial interpolation\"\n"
    "AHFinderDirect::geometry_interpolator_pars             = \"order=4\"\n"
    "AHFinderDirect::initial_guess__coord_sphere__radius[1] = 0.5\n"
    "AHFinderDirect::initial_guess_method[1]                = \"coordinate sphere\"\n"
    "AHFinderDirect::max_Newton_iterations__initial         = 500\n"
    "AHFinderDirect::max_Newton_iterations__subsequent      = 10\n"
    "AHFinderDirect::N_horizons                             = 1\n"
    "AHFinderDirect::output_BH_diagnostics                  = \"yes\"\n"
    "AHFinderDirect::reset_horizon_after_not_finding[1]     = \"no\"\n"
    "AHFinderDirect::set_mask_for_individual_horizon[1]     = \"no\"\n"
    "AHFinderDirect::surface_interpolator_name              = \"Lagrange polynomial interpolation\"\n"
    "AHFinderDirect::surface_interpolator_pars              = \"order=4\"\n"
    "AHFinderDirect::verbose_level                          = \"algorithm details\"\n"
    "AHFinderDirect::which_surface_to_store_info[1]         = 0\n"
    "AHFinderDirect::run_at_CCTK_POSTSTEP = false\n"
    "AHFinderDirect::run_at_CCTK_ANALYSIS = true\n"
    "SphericalSurface::nsurfaces = 1\n"
    "SphericalSurface::maxntheta = 79\n"
    "SphericalSurface::maxnphi = 146\n"
    "SphericalSurface::ntheta      [0] = 79\n"
    "SphericalSurface::nphi        [0] = 146\n"
    "SphericalSurface::nghoststheta[0] = 2\n"
    "SphericalSurface::nghostsphi  [0] = 2\n";

  /* let Cactus create the grid etc */
  cGH *cctkGH = initCactus(*argv[0],ADM_gridfunctions);

    
  /* do something with the grid */
  double *gxx = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::gxx"));
  double *gxy = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::gxy"));
  double *gxz = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::gxz"));
  double *gyy = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::gyy"));
  double *gyz = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::gyz"));
  double *gzz = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::gzz"));

  double *kxx = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::kxx"));
  double *kxy = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::kxy"));
  double *kxz = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::kxz"));
  double *kyy = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::kyy"));
  double *kyz = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::kyz"));
  double *kzz = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "ADMBase::kzz"));

#define ETK_IDX(i,j,k) ((i) + cctkGH->cctk_ash[0] *( (j) + cctkGH->cctk_ash[1] * (k)))
#define IDX4pt_for_ETK(g,idx)   ( (idx) + (cctkGH->cctk_lsh[0]*cctkGH->cctk_lsh[1]*cctkGH->cctk_lsh[2]) * (g) )

  for(int i=0;i<cctkGH->cctk_lsh[0]*cctkGH->cctk_lsh[1]*cctkGH->cctk_lsh[2];i++) {
    gxx[i] = ADM_gridfunctions[IDX4pt_for_ETK(GXXGF,i)];
    gxy[i] = ADM_gridfunctions[IDX4pt_for_ETK(GXYGF,i)];
    gxz[i] = ADM_gridfunctions[IDX4pt_for_ETK(GXZGF,i)];
    gyy[i] = ADM_gridfunctions[IDX4pt_for_ETK(GYYGF,i)];
    gyz[i] = ADM_gridfunctions[IDX4pt_for_ETK(GYZGF,i)];
    gzz[i] = ADM_gridfunctions[IDX4pt_for_ETK(GZZGF,i)];

    kxx[i] = ADM_gridfunctions[IDX4pt_for_ETK(KXXGF,i)];
    kxy[i] = ADM_gridfunctions[IDX4pt_for_ETK(KXYGF,i)];
    kxz[i] = ADM_gridfunctions[IDX4pt_for_ETK(KXZGF,i)];
    kyy[i] = ADM_gridfunctions[IDX4pt_for_ETK(KYYGF,i)];
    kyz[i] = ADM_gridfunctions[IDX4pt_for_ETK(KYZGF,i)];
    kzz[i] = ADM_gridfunctions[IDX4pt_for_ETK(KZZGF,i)];
  }

  CCTK_Evolve(&ConfigData);

  double *xx = static_cast<double*>(CCTK_VarDataPtr(cctkGH, 0, "grid::x"));

  for(int i=0;i<cctkGH->cctk_lsh[0];i++) {
    //printf("%e\n",gzz[ETK_IDX(i,5,5)]);
    //printf("%e\n",xx[ETK_IDX(i,5,5)]);
  }

  /* tear everything down */
  finalizeCactus();

  return 0;
}
